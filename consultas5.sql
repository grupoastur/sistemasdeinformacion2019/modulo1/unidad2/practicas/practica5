﻿﻿USE federacion;

-- 1 Nombrar el nombre de la persona que tiene cargo  de presidente.
SELECT m.nombre_m FROM miembro m JOIN composicion c ON m.dni = c.dni WHERE c.cargo='presidente';

-- 2 Listar la dirección de la federación en la que el cargo sea gerente
  SELECT f.direccion FROM federacion f JOIN composicion c ON f.nombre = c.nombre WHERE c.cargo='gerente'; 

-- 3 Listar todos los nombres de las federaciónes que no tengan el cargo de asesor técnico
  SELECT c.nombre FROM composicion c WHERE c.cargo='asesor_tecnico'; 
  SELECT f.nombre FROM federacion f LEFT JOIN (SELECT c.nombre FROM composicion c WHERE c.cargo='asesor_tecnico') c1
  ON c1.nombre= f.nombre WHERE c1.nombre IS NULL; 

-- 4 Listar el nombre y el cargo de la tabla composición dividido entre el cargo de la tabla composición
SELECT * FROM federacion f;

-- 5 listar el nombre de la tabla composición con el cargo de asesor técnico haciendo intersección con el nombre de la misma tabla 
-- con cargo psicólogo
SELECT c1.nombre FROM 
(SELECT c.nombre FROM composicion c WHERE c.cargo='asesor_tecnico') c1 JOIN 
(SELECT c.nombre FROM composicion c WHERE c.cargo='psicologo') c2 ON c1.nombre= c2.nombre;