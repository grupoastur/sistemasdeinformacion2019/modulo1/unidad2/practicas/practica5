﻿DROP DATABASE IF EXISTS federacion;
CREATE DATABASE IF NOT EXISTS federacion;
USE federacion;

CREATE TABLE federacion (
nombre varchar(20),
direccion varchar(40),
telefono int,
PRIMARY KEY (nombre) 
);
CREATE TABLE miembro (
dni int,
nombre_m varchar(40),
titulacion varchar(20),
PRIMARY KEY (dni)
);
CREATE TABLE composicion (
cargo varchar(20),
fecha_inicio date,
nombre varchar(20),
dni int,
PRIMARY KEY (nombre, dni),
CONSTRAINT nomfede FOREIGN KEY (nombre) REFERENCES federacion(nombre),
CONSTRAINT dnimiembro FOREIGN KEY (dni) REFERENCES miembro(dni)
);