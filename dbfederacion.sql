﻿DROP DATABASE IF EXISTS federacion;
CREATE DATABASE IF NOT EXISTS federacion;
USE federacion;

CREATE TABLE federacion(
nombre varchar (30),
  direccion varchar (40),
  telefono int,
  PRIMARY KEY (nombre)
);
CREATE TABLE miembro (
dni int,
  nombre_m varchar(30),
  titulacion varchar(20),
  PRIMARY KEY(dni)
);
CREATE TABLE composicion(
nombre varchar(30),
  dni int,
  cargo varchar(20),
  fecha_inicio date,
  PRIMARY KEY (nombre, dni),
  CONSTRAINT compfeder FOREIGN KEY(nombre) REFERENCES federacion (nombre),
  CONSTRAINT compmiembro FOREIGN KEY(dni) REFERENCES miembro (dni)
);


USE federacion;

<<<<<<< HEAD
INSERT INTO federacion(nombre, direccion, telefono) VALUES
('lapedraja','av_la_medalla_nº3','654938219'),
=======
INSERT INTO federacion (nombre, direccion, telefono) VALUES
('lapedraja', 'av_la_medalla_nº3','654938219'),
>>>>>>> f60a4e3657e0dc66b62c9b95160aec0173e6fc6d
('eljuanete','calle_norte_bajo','432857243'),
('fonsoriorisorio','calle_fonsin_5_entre','809576897'),
('sulfurminosa','barrio_deltebre_nº8_izq','876098432'),
('vitorinosa','calle_pereda_nº6','665998009'),
('vertederos','barriada_dosantos_5','554223998')
;

INSERT INTO miembro(dni, nombre_m, titulacion) VALUES
  ('48578934z', 'javier', 'economicas'),
<<<<<<< HEAD
  ('57633489k', 'rigoberto','gerente'),
  ('66599832l','saturnin', 'tecnico'),
  ('79420857f', 'alfredo', 'psicologo'),
  ('88570236a', 'augusto', 'veterinario'),
  ('66433219c', 'jorge','sacerdote'),
  ('76548763a','juanma','operador')
=======
  ('57633489k', 'rigoberto','catedra'),
  ('66599832l','saturnin', 'maca'),
  ('79420857f', 'alfredo', 'comercial'),
  ('88570236a', 'augusto', 'veterinario'),
  ('77600422b', 'marco', 'baron')
>>>>>>> f60a4e3657e0dc66b62c9b95160aec0173e6fc6d
  ;

INSERT INTO composicion(nombre, dni, cargo, fecha_inicio) VALUES
  ('lapedraja','48578934z','presidente','12-04-92'),
<<<<<<< HEAD
  ('eljuanete','57633489k','superintendente','04-07-98'),
  ('fonsoriorisorio','66599832l','ayudante','16-11-99'),
  ('sulfurminosa','79420857f','asesor_tecnico','22-10-2001'),
  ('vitorinosa','88570236a','enseñante','30-12-2003'),
  ('vertederos','66433219c','mediador','03-02-1984'),
  ('lapedraja','79420857f','psicologo','12-08-92'),
  ('sulfurminosa','76548763a','psicologo','22-10-2001')
=======
  ('eljuanete','57633489k','gerente','25-08-03'),
  ('fonsoriorisorio','66599832l','asesor_tecnico','30-03-01'),
  ('sulfurminosa','79420857f','jefe_ventas','22-11-99'),
  ('vitorinosa','88570236a','psicologo','01-01-01'),
  ('vertederos','77600422b','director_marketing','11-05-94')
>>>>>>> f60a4e3657e0dc66b62c9b95160aec0173e6fc6d
  ;
